# Files related to Mikuláš Poul's Master's Thesis

See details of the thesis on [this site](https://masters-thesis.mikulaspoul.cz/). That page includes links to the repository of the code as well as links to the datasets used.
The file `GIT_README.md` contains the README from that repository with information about the files included here.
 