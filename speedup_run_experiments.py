"""
This file runs the speedup experiments.
"""
import sys
from pathlib import Path
from collections import Counter

import numpy as np
import pandas as pd

from tqdm import tqdm, trange

import springbok
from springbok.utils import measure
from table_utils import get_graph_for_table

RESULTS_FILE_RAW = Path("outputs/speedup_table_raw_runs.csv")


def save_result(name, tp, run, avg_path_length, debug_information, measure_output):
    if not RESULTS_FILE_RAW.exists():
        RESULTS_FILE_RAW.write_text(";".join(
            ["Name", "Type", "Trial", "AVG distance", "Nodes",
             "Edges", "1core size", "2chain size", "Reduced nodes", "Reduced edges",
             "Real time", "User time", "Sys time",
             ]
        ) + "\n")

    with RESULTS_FILE_RAW.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            name, tp, run, avg_path_length,
            debug_information["original_nodes"],
            debug_information["original_edges"],
            debug_information.get("1core_size", ""),
            debug_information.get("2chain_size", ""),
            debug_information.get("reduced_nodes", ""),
            debug_information.get("reduced_edges", ""),
            measure_output["real"],
            measure_output["user"],
            measure_output["sys"],
        ]) + "\n")


def summarize_speedup_table():
    df_raw = pd.read_csv(str(RESULTS_FILE_RAW), delimiter=";")

    names = set(df_raw.Name)

    summarized_results = []
    tps = ["basic", "basic_weighted", "speedup", "speedup_weighted"]

    for name in names:
        df_raw_name = df_raw[df_raw.Name == name]
        try:
            first_speedup = df_raw_name[df_raw.Type == "speedup_weighted"].iloc[0]
        except Exception:
            continue

        results = {
            "Name": name
        }

        for x in ["AVG distance", "Nodes", "Edges",
                  "1core size", "2chain size", "Reduced nodes", "Reduced edges"]:
            results[x] = first_speedup[x]

        results[f"AVG distance matches"] = len(df_raw_name["AVG distance"].round(5).unique()) == 1

        for tp in tps:
            results[f"{tp} avg real"] = df_raw_name[df_raw_name.Type == tp]["Real time"].mean()
            results[f"{tp} std real"] = df_raw_name[df_raw_name.Type == tp]["Real time"].std()
            results[f"{tp} avg user"] = df_raw_name[df_raw_name.Type == tp]["User time"].mean()
            results[f"{tp} std user"] = df_raw_name[df_raw_name.Type == tp]["User time"].std()

        results["Speedup - user"] = results["basic avg user"] / results["speedup avg user"]
        results["Speedup (weight) - user"] = results["basic_weighted avg user"] / results["speedup_weighted avg user"]

        results["Speedup - real"] = results["basic avg real"] / results["speedup avg real"]
        results["Speedup (weight) - real"] = results["basic_weighted avg real"] / results["speedup_weighted avg real"]

        summarized_results.append(results)

    results = pd.DataFrame(summarized_results, columns=[
        'Name',
        'Nodes',
        'Edges',
        '1core size',
        '2chain size',
        'Reduced nodes',
        'Reduced edges',
        'AVG distance matches',
        'Speedup - real',
        'Speedup (weight) - real',

        'Speedup - user',
        'Speedup (weight) - user',

        'AVG distance',
        'basic avg real',
        'basic avg user',
        'basic_weighted avg real',
        'basic_weighted avg user',
        'speedup avg real',
        'speedup avg user',
        'speedup_weighted avg real',
        'speedup_weighted avg user'

        'basic std real',
        'basic std user',
        'basic_weighted std real',
        'basic_weighted std user',
        'speedup std real',
        'speedup std user',
        'speedup_weighted std real',
        'speedup_weighted std user',
    ])

    for x, v in {
        "1core size": np.int,
        "2chain size": np.int,
        "Reduced edges": np.int,
        "Reduced nodes": np.int,
    }.items():
        results[x] = results[x].astype(v)

    results.sort_values("Name", inplace=True)

    results.to_csv("outputs/speedup_table.csv", index=False)


def get_already_processed():
    try:
        df = pd.read_csv("outputs/speedup_table_raw_runs.csv", delimiter=";")
    except Exception:
        return {}

    counter = Counter(df["Name"])
    already_processed = {}

    for k, v in counter.items():
        already_processed[k] = {
            "runs": v // 4,
            "skip_basic": v % 4 >= 1,
            "skip_basic_weighted": v % 4 >= 2,
            "skip_speedup": v % 4 >= 3,
            "skip_speedup_weighted": False,
        }
    return already_processed


if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "--summarize":
        summarize_speedup_table()

        sys.exit(0)

    output = {}

    graphs_to_test = [
        "as-22july06",
        "cond-mat-2003",
        Path("test_graphs/ba-graph.gt"),
        Path("test_graphs/er-graph.gt"),
    ]

    already_processed = get_already_processed()

    for graph_name in tqdm(graphs_to_test):

        graph_name, graph = get_graph_for_table(graph_name)

        weights_float = graph.new_edge_property("float")
        weights_float.a = np.ones(graph.num_edges(), dtype=np.float)

        already_processed_this_graph = already_processed.get(graph_name, {})
        already_ran = already_processed_this_graph.get("runs", 0)

        runs = 10

        for run in trange(already_ran, runs, leave=False, desc=graph_name):
            # init because ground truth doesn't do debug information
            debug_information = {"original_nodes": graph.num_vertices(), "original_edges": graph.num_edges()}

            if not already_processed_this_graph.get("skip_basic", False):
                with measure(output=output):
                    try:
                        result = springbok.naive_average_path_length(graph, skip_checks=True)
                    except RuntimeError:
                        print(f"skipping {graph_name}")
                        break
                save_result(graph_name, "basic", run, result, debug_information, output)

            if not already_processed_this_graph.get("skip_basic_weighted", False):
                with measure(output=output):
                    result = springbok.naive_average_path_length(graph, weights=weights_float, skip_checks=True)
                save_result(graph_name, "basic_weighted", run, result, debug_information, output)

            if not already_processed_this_graph.get("skip_speedup", False):
                with measure(output=output):
                    result = springbok.average_path_length(graph, skip_checks=True,
                                                           debug_information=debug_information)
                save_result(graph_name, "speedup", run, result, debug_information, output)

            if not already_processed_this_graph.get("skip_speedup_weighted", False):
                with measure(output=output):
                    result = springbok.average_path_length(graph, weights=weights_float, skip_checks=True,
                                                           debug_information=debug_information)
                save_result(graph_name, "speedup_weighted", run, result, debug_information, output)

            for x in "basic", "basic_weighted", "speedup", "speedup_weighted":
                already_processed_this_graph[f"skip_{x}"] = False
