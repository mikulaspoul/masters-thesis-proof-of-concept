"""
This file runs the speed experiments of sampling.
"""
import decimal
from collections import Counter
from pathlib import Path

import pandas as pd
import numpy as np
from tqdm import tqdm, trange

import springbok
from springbok.utils import measure
from table_utils import get_graph_for_table
from sampling_run_experiments import sampling_sizes, sampling_sizes_reduced_str

RESULTS_FILE_RAW = Path("outputs/sampling_speed.csv")

target_runs = {
    "unweighted": 10,
    "weighted": 10,
}


def save_result(graph_name, tp, run, sample, measure_output):
    if not RESULTS_FILE_RAW.exists():
        RESULTS_FILE_RAW.write_text(";".join(
            ["Name", "Type", "Trial", "Sample size",
             "Real time", "User time", "Sys time",
             ]
        ) + "\n")

    with RESULTS_FILE_RAW.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            graph_name, tp, run, sample,
            measure_output["real"],
            measure_output["user"],
            measure_output["sys"],
        ]) + "\n")


def get_already_processed():
    try:
        df = pd.read_csv(RESULTS_FILE_RAW,
                         delimiter=";",
                         usecols=["Name", "Type", "Sample size"],
                         dtype={"Sample size": str})
    except Exception:
        return {}

    return Counter(tuple(r[["Name", "Type", "Sample size"]]) for i, r in df.iterrows())


def get_weights(graph):
    unit = graph.new_ep("double")
    unit.a = np.ones(graph.num_edges())

    return unit


def desc(x):
    return x.ljust(40)


if __name__ == "__main__":

    graphs_to_test = [
        "as-22july06",
        "cond-mat-2003",
        Path("test_graphs/ba-graph.gt"),
        Path("test_graphs/er-graph.gt"),
    ]

    already_processed = get_already_processed()

    for weighted in tqdm([False, True], desc=desc("Weighted")):
        for method in tqdm(["ru", "rp", "ou"], desc=desc(f"Methods for {weighted}")):

            for graph_name in tqdm(graphs_to_test, desc=desc(f"Graph for {weighted}, {method}")):

                graph_name, graph = get_graph_for_table(graph_name)

                if weighted:
                    weights = get_weights(graph)
                    tp = f"weighted_{method}"
                else:
                    weights = None
                    tp = f"unweighted_{method}"

                ss = sampling_sizes
                if weighted and graph_name == "er-graph":
                    ss = [decimal.Decimal(x) for x in sampling_sizes_reduced_str]

                for run in trange(target_runs["weighted" if weighted else "unweighted"], leave=False,
                                  desc=desc(f"Run for {weighted}, {method}, {graph_name}")):

                    for sample in tqdm(ss, leave=False,
                                       desc=desc(f"Sample for {weighted}, {method}, {graph_name}, {run}")):

                        kwargs = {"sample": sample, "sample_method": method}

                        if already_processed.get((graph_name, tp, str(sample)), 0) > run:
                            continue

                        output = {}
                        with measure(output=output):
                            springbok.average_path_length(graph, **kwargs)

                        save_result(graph_name, tp, run, sample, output)
