"""
This file tests that the naive APL implementation works correct compared to a python implementation and that it's faster.
"""
import numpy as np
from graph_tool import collection
from graph_tool import topology
import springbok
import timeit


def get_true_average_path_length(graph):
    distances = topology.shortest_distance(graph)

    sum_of_distances = 0
    total_distances = 0
    eccentricities = []

    for distance in distances:
        eccentricities.append(np.max(distance.a))
        sum_of_distances += np.sum(distance.a)
        total_distances += graph.num_vertices() - 1

    return sum_of_distances / total_distances, np.min(eccentricities), np.max(eccentricities)


if __name__ == "__main__":
    graph = collection.data["as-22july06"]

    print(get_true_average_path_length(graph))
    print(springbok.naive_average_path_length(graph))

    print(timeit.timeit(lambda: get_true_average_path_length(graph), number=5))
    print(timeit.timeit(lambda: springbok.naive_average_path_length(graph), number=5))
