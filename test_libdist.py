"""
This file tests the functionality of libdist, the util for shortest distances withtin a graph from a list of sources
"""
import numpy as np
from graph_tool import collection, topology

import springbok._dist

graph = collection.data["adjnoun"]
graph = topology.extract_largest_component(graph, prune=True)

weights = graph.new_edge_property("int")
weights.a = np.ones(graph.num_edges(), dtype=np.int)

weights_float = graph.new_edge_property("float")
weights_float.a = np.ones(graph.num_edges(), dtype=np.float)

x = springbok._dist.shortest_distance(graph)

print(springbok._dist.shortest_distance(graph, weights=weights).get_2d_array(list(range(graph.num_vertices()))))

print(topology.shortest_distance(graph).get_2d_array(list(range(graph.num_vertices()))))
print(topology.shortest_distance(graph, weights=weights).get_2d_array(list(range(graph.num_vertices()))))

y = springbok._dist.shortest_distance(graph, source=[0, 1, 2, 4])
print(x[0] == y[0])
print(y[3])
print(y[4])

y = springbok._dist.shortest_distance(graph, source=[0, 1, 2, 4], weights=weights)
print(x[0] == y[0])
print(y[3])
print(y[4])

y = springbok._dist.shortest_distance(graph, source=[0, 1, 2, 4], weights=weights_float)
print(x[0] == y[0])
print(y[3])
print(y[4])
