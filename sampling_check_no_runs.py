"""
This file checks how many runs of each sampling experiments were run and
if the desired count isn't reached writes to runner.sh with the commands to run them.
"""
from pathlib import Path

import pandas as pd

from sampling_run_experiments import (RESULTS_FILE_RAW, RESULTS_FILE_RAW_WEIGHTED, sampling_sizes, target_runs,
                                      sampling_sizes_reduced_str)

ss = len(sampling_sizes)
sr = len(sampling_sizes_reduced_str)


def runs(df, graph_name=None):
    if graph_name == "er-graph":
        return len(df) / sr
    return len(df) / ss


def parse_type(tp):
    return tp.split("_")


if __name__ == "__main__":
    # unweighted
    df = pd.read_csv(RESULTS_FILE_RAW, delimiter=";",
                     usecols=["Name", "Trial", "Sample method"])


    graph_names = ["ba-graph", "as-22july06", "cond-mat-2003", "er-graph"]

    results = []

    for graph_name in graph_names:
        graph = df[df["Name"] == graph_name]

        results.extend([
            [graph_name, None, "ru", runs(graph[graph["Sample method"] == "ru"])],
            [graph_name, None, "rp", runs(graph[graph["Sample method"] == "rp"])],
            [graph_name, None, "ou", runs(graph[graph["Sample method"] == "ou"])],
        ])

    # weighted
    df = pd.read_csv(RESULTS_FILE_RAW_WEIGHTED, delimiter=";",
                     usecols=["Name", "Type", "Trial"])

    types = df["Type"].unique()

    for graph_name in graph_names:
        graph = df[df["Name"] == graph_name]

        for tp in types:
            tp_df = graph[graph["Type"] == tp]

            results.append([graph_name] + list(parse_type(tp)) + [runs(tp_df, graph_name)])

    # process results
    df = pd.DataFrame(results, columns=["Name", "Weights", "Method", "Runs"])

    df.sort_values(["Name", "Method", "Weights"], inplace=True)

    print(df)

    # create the runner file
    to_run = set()

    for i, row in df.iterrows():
        weights_type = None

        weighted = row["Weights"] is not None

        if weighted:
            weights_type = row["Weights"]

        full = row["Method"] == "ou"

        runs = target_runs["weighted" if weighted else "unweighted"]
        if weighted:
            runs = runs["full" if full else "reduced"]

        if row["Runs"] < runs:
            to_run.add(
                (weighted, weights_type, row["Method"])
            )

    commands = []

    for r in to_run:
        weighted, weights_type, method = r

        command = "python sampling_run_experiments.py"

        if weighted:
            command += f" --weighted --{weights_type}"

        if method != "ru":
            command += f" --sample-{method}"

        commands.append(command)

    commands.sort()

    commands = "#!/bin/bash\n\n" + "\n".join(commands)

    print(commands)

    Path("runner.sh").write_text(commands)
