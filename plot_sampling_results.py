"""
This file plots the results of sampling, the absolute values
compared to the true value, the min/max/mean/std of the approximation.
"""
from pprint import pprint

import pandas as pd
from matplotlib import pyplot as plt
import sns_config  # noqa

from sampling_run_experiments import (RESULTS_FILE_RAW, RESULTS_FILE_RAW_WEIGHTED, RESULTS_FILE_TRUE_VALUES)


sampling_sizes = [
    "0.0010", "0.0020", "0.0030", "0.0040", "0.0050", "0.0060", "0.0070", "0.0080", "0.0090",
    "0.0100", "0.0200", "0.0300", "0.0400", "0.0500", "0.0600", "0.0700", "0.0800", "0.0900", "0.1000",
    "0.1100", "0.1200", "0.1300", "0.1400", "0.1500", "0.1600", "0.1700", "0.1800", "0.1900",
    "0.20", "0.25", "0.30", "0.35", "0.40", "0.45", "0.50", "0.55",
    "0.60", "0.65", "0.70", "0.75", "0.80", "0.85", "0.90", "0.95",
]

sampling_sizes_reduced = [
    "0.0010", "0.0030", "0.0040", "0.0060", "0.0080", "0.0090",
    "0.0100", "0.0300", "0.0400", "0.0600", "0.0800", "0.0900", "0.1000",
    "0.1100", "0.1300", "0.1400", "0.1600", "0.1800", "0.1900",
    "0.20", "0.30", "0.40", "0.50",
    "0.60", "0.70", "0.80", "0.90",
]



def load_df(weighted, weights_type, method):
    results_path = RESULTS_FILE_RAW_WEIGHTED if weighted else RESULTS_FILE_RAW

    dtype = {"Sample size": str}

    df = pd.read_csv(results_path, delimiter=";", dtype=dtype)

    if weighted:
        df = df[df["Type"] == f"{weights_type}_{method}"]
        file_appendix = f"_weighted_{weights_type}_{method}"
    else:
        df = df[df["Sample method"] == method]
        file_appendix = f"_unweighted_{method}"

    return df, file_appendix


def plot_absolute(weighted, weights_type, method, true_values, axis_limits):
    print("absolute", weighted, weights_type, method)

    df, file_appendix = load_df(weighted, weights_type, method)

    graph_names = df["Name"].unique()

    processed = []

    for graph_name in graph_names:
        for sample in (sampling_sizes_reduced if (graph_name == "er-graph" and weighted) else sampling_sizes):
            x = df[(df["Name"] == graph_name) & (df["Sample size"] == sample.rstrip("0"))]

            processed.append([graph_name, sample,
                              x.iloc[0]["Original nodes"], x.iloc[0]["Reduced nodes"], x.iloc[0]["Sampled nodes"],
                              x["Counted nodes"].mean(),
                              x["Approximation"].mean(), x["Approximation"].std(), x["Approximation"].min(),
                              x["Approximation"].max(),
                              x["Real time"].mean(), x["User time"].mean()
                              ])

    stats_absolute = pd.DataFrame(processed, columns=[
        "Name", "Sample size", "Original", "Reduced", "Sampled", "Counted (mean)",
        "Approx mean", "Approx std", "Approx min", "Approx max", "Mean real time", "Mean user time",
    ])
    stats_absolute["Counted (mean)"] = stats_absolute["Counted (mean)"].round(2)

    for graph_name in graph_names:
        d = stats_absolute[stats_absolute["Name"] == graph_name]

        sizes = d["Sample size"].astype(float)
        means = d["Approx mean"]
        std = d["Approx std"]
        mins = d["Approx min"]
        maxes = d["Approx max"]

        fig, ax = plt.subplots(figsize=(5, 5))

        ax.axhline(true_values[graph_name], label="True value", color="r")

        ax.plot(sizes, means, label="Mean")
        ax.plot(sizes, maxes, label="Max")
        ax.plot(sizes, mins, label="Min")
        ax.errorbar(sizes, means, std, fmt=' k', ecolor='gray', lw=1, label="SD", capsize=2)

        ax.set_xscale("log")

        ax.set_xlabel("Sample size")
        ax.set_ylabel("Approximation")

        ax.legend()

        top = axis_limits[graph_name]["top"]
        bottom = axis_limits[graph_name]["bottom"]

        ax.set_ylim(top=top * 1.01, bottom=bottom * 0.99)

        fig.tight_layout()
        fig.savefig(f"figures/sampling/absolute_{graph_name}{file_appendix}.pdf")
        plt.close(fig)

    return stats_absolute


def _true_values(df):
    graph_names = df["Name"].unique()

    true_values = {}
    for graph_name in graph_names:
        d = df[df["Name"] == graph_name]

        true_values[graph_name] = float(d.iloc[0]["True value"])

    return true_values


def get_true_values():
    df = pd.read_csv(RESULTS_FILE_TRUE_VALUES, delimiter=";")

    return {
        "weighted": {
            "uniform": _true_values(df[df["Weights"] == "uniform"]),
            "unit": _true_values(df[df["Weights"] == "unit"]),
            "normal": _true_values(df[df["Weights"] == "normal"])
        },
        "unweighted": _true_values(df[df["Weights"] == "None"])
    }


def get_axis_limits(weights):
    results_path = RESULTS_FILE_RAW_WEIGHTED if weights else RESULTS_FILE_RAW
    df = pd.read_csv(results_path, usecols=["Name", "Approximation", "Sample size"] + ([] if not weights else ["Type"]),
                     delimiter=";")

    df = df[df["Sample size"] >= 0.001]

    if weights:
        df = df[df["Type"].isin([f"{weights}_ou", f"{weights}_rp", f"{weights}_ru"])]

    graph_names = df["Name"].unique()

    limits = {}

    for graph_name in graph_names:
        d = df[df["Name"] == graph_name]

        limits[graph_name] = {
            "top": d["Approximation"].max(),
            "bottom": d["Approximation"].min(),
        }

    return limits


if __name__ == "__main__":
    true_values = get_true_values()
    pprint(true_values)

    limits = get_axis_limits(None)

    for method in ["ou", "rp", "ru"]:
        plot_absolute(False, None, method, true_values["unweighted"], limits)

    for weights_type in ["unit", "uniform", "normal"]:

        limits = get_axis_limits(weights_type)

        for method in ["ou", "rp", "ru"]:
            plot_absolute(True, weights_type, method, true_values["weighted"][weights_type], limits)
