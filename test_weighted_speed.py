"""
This file tests the speed of finding the APL of weighted graphs (with different type of weights).
"""
import numpy as np
from graph_tool import collection, topology

import springbok
from springbok.utils import measure

graph = collection.data["cond-mat"]
graph = topology.extract_largest_component(graph, prune=True)

weights = graph.new_edge_property("int")
weights.a = np.arange(graph.num_edges()) + 1

real = {}
user = {}

output = {}
with measure("g w/o", output):
    print(springbok.naive_average_path_length(graph))
real["g w/o"] = output["real"]
user["g w/o"] = output["user"]

with measure("g w/int", output):
    print(springbok.naive_average_path_length(graph, weights=weights))
real["g w/int"] = output["real"]
user["g w/int"] = output["user"]

weights = graph.new_edge_property("float")
weights.a = np.arange(graph.num_edges()) + 1

with measure("g w/float", output):
    print(springbok.naive_average_path_length(graph, weights=weights))
real["g w/float"] = output["real"]
user["g w/float"] = output["user"]

print("real", real)
print("user", user)
