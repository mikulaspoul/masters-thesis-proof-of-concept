all:
	make -C springbok ALL

.PHONY: submit.zip
submit.zip:
	rm -rf submit.zip

	zip -r submit.zip outputs tables requirements.txt LICENSE *.py *.ipynb
	zip -ur submit.zip figures  -x '*/.ipynb_checkpoints/*'
	zip -ur submit.zip springbok -x '*/.idea/*' -x '*/__pycache__/*' -x '*/cmake-build-debug/*' -x '*.so'

	zip -ur submit.zip README.md
	printf "@ README.md\n@=GIT_README.md\n" | zipnote -w submit.zip

	zip -ur submit.zip ZIP_README.md
	printf "@ ZIP_README.md\n@=README.md\n" | zipnote -w submit.zip

	zip -ur submit.zip test_graphs/as-22july06.gt test_graphs/as-22july06_weights.npz
	zip -ur submit.zip test_graphs/ba-graph.gt test_graphs/ba-graph_weights.npz
	zip -ur submit.zip test_graphs/cond-mat-2003.gt test_graphs/cond-mat-2003_weights.npz
	zip -ur submit.zip test_graphs/er-graph.gt test_graphs/er-graph_weights.npz
