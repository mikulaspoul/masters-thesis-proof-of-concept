"""
This file tests finding the APL of weighted graphs.
"""
from pprint import pprint

import numpy as np
from graph_tool import collection, topology

import springbok
from springbok.utils import measure

graph = collection.data["cond-mat"]
graph = topology.extract_largest_component(graph, prune=True)

weights_int = graph.new_edge_property("int")
weights_int.a = np.ones(graph.num_edges(), dtype=int)
weights_float = graph.new_edge_property("float")
weights_float.a = np.ones(graph.num_edges(), dtype=int)

user = {"basic": {}, "faster": {}}
real = {"basic": {}, "faster": {}}
results = {}

output = {}


def update(name, output, result, t="basic"):
    user[t][name] = output["user"]
    real[t][name] = output["real"]
    results[f"{t} {name}"] = result


with measure("g int", output):
    result = springbok.naive_average_path_length(graph, weights=weights_int, skip_checks=True)
update("int", output, result)
with measure("a int", output):
    result = springbok.average_path_length(graph, weights=weights_int, skip_checks=True)
update("int", output, result, "faster")

with measure("g None", output):
    result = springbok.naive_average_path_length(graph, skip_checks=True)
update("None", output, result)
with measure("a None", output):
    result = springbok.average_path_length(graph, skip_checks=True)
update("None", output, result, "faster")

with measure("g float", output):
    result = springbok.naive_average_path_length(graph, weights=weights_float, skip_checks=True)
update("float", output, result)
with measure("a float", output):
    result = springbok.average_path_length(graph, weights=weights_float, skip_checks=True)
update("float", output, result, "faster")

print("real")
print(real)
print("user")
print(user)
pprint(results)
