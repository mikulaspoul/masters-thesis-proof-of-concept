"""
This file runs the experiments of sampling (all combos of weighted
and unweighted and methods) and calculates the true values.
"""
import decimal
import sys
from collections import Counter
from pathlib import Path

import pandas as pd
import numpy as np
from tqdm import tqdm, trange

import springbok
from springbok.utils import measure
from table_utils import get_graph_for_table

RESULTS_FILE_RAW = Path("outputs/sampling_table_raw_runs.csv")
RESULTS_FILE_RAW_WEIGHTED = Path("outputs/sampling_weighted_table_raw_runs.csv")
RESULTS_FILE_TRUE_VALUES = Path("outputs/sampling_true_values.csv")

sampling_sizes_str = [
    "0.0001", "0.0002", "0.0003", "0.0004", "0.0005", "0.0006", "0.0007", "0.0008", "0.0009",
    "0.0010", "0.0020", "0.0030", "0.0040", "0.0050", "0.0060", "0.0070", "0.0080", "0.0090",
    "0.0100", "0.0200", "0.0300", "0.0400", "0.0500", "0.0600", "0.0700", "0.0800", "0.0900", "0.1000",
    "0.1100", "0.1200", "0.1300", "0.1400", "0.1500", "0.1600", "0.1700", "0.1800", "0.1900",
    "0.20", "0.25", "0.30", "0.35", "0.40", "0.45", "0.50", "0.55",
    "0.60", "0.65", "0.70", "0.75", "0.80", "0.85", "0.90", "0.95",
]

sampling_sizes_reduced_str = [
    "0.0001", "0.0003", "0.0004", "0.0006", "0.0008", "0.0009",
    "0.0010", "0.0030", "0.0040", "0.0060", "0.0080", "0.0090",
    "0.0100", "0.0300", "0.0400", "0.0600", "0.0800", "0.0900", "0.1000",
    "0.1100", "0.1300", "0.1400", "0.1600", "0.1800", "0.1900",
    "0.20", "0.30", "0.40", "0.50",
    "0.60", "0.70", "0.80", "0.90",
]

sampling_sizes = [decimal.Decimal(x) for x in sampling_sizes_str]

target_runs = {
    "unweighted": 40,
    "weighted": {
        "full": 20,
        "reduced": 20,
    }
}


def save_result(graph_name, run, sample, result, debug_information, measure_output, sample_method):
    if not RESULTS_FILE_RAW.exists():
        RESULTS_FILE_RAW.write_text(";".join(
            ["Name", "Trial", "Sample size", "Approximation",
             "Original nodes", "Reduced nodes", "Sampled nodes", "Counted nodes",
             "Real time", "User time", "Sys time", "Sample method",
             ]
        ) + "\n")

    with RESULTS_FILE_RAW.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            graph_name, run, sample, result,
            debug_information.get("original_nodes", ""),
            debug_information.get("reduced_nodes", ""),
            debug_information.get("sampled_nodes", ""),
            debug_information.get("counted_nodes", ""),
            measure_output["real"],
            measure_output["user"],
            measure_output["sys"],
            sample_method,
        ]) + "\n")


def save_result_weighted(graph_name, run, sample, result, debug_information, measure_output, tp):
    if not RESULTS_FILE_RAW_WEIGHTED.exists():
        RESULTS_FILE_RAW_WEIGHTED.write_text(";".join(
            ["Name", "Type", "Trial", "Sample size", "Approximation",
             "Original nodes", "Reduced nodes", "Sampled nodes", "Counted nodes",
             "Real time", "User time", "Sys time",
             ]
        ) + "\n")

    with RESULTS_FILE_RAW_WEIGHTED.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            graph_name, tp, run, sample, result,
            debug_information.get("original_nodes", ""),
            debug_information.get("reduced_nodes", ""),
            debug_information.get("sampled_nodes", ""),
            debug_information.get("counted_nodes", ""),
            measure_output["real"],
            measure_output["user"],
            measure_output["sys"],
        ]) + "\n")


def get_already_processed():
    try:
        df = pd.read_csv(RESULTS_FILE_RAW,
                         delimiter=";",
                         usecols=["Name", "Sample size", "Sample method"],
                         dtype={"Name": str,  "Sample size": str, "Sample method": str})
    except Exception:
        return {}

    return Counter(tuple(r[["Name", "Sample prob", "Sample method"]]) for i, r in df.iterrows())


def get_already_processed_weighted():
    try:
        df = pd.read_csv(RESULTS_FILE_RAW_WEIGHTED,
                         delimiter=";",
                         usecols=["Name", "Type", "Sample size"],
                         dtype={"Name": str, "Type": str, "Sample size": str})
    except Exception:
        return {}

    return Counter(tuple(r[["Name", "Type", "Sample size"]]) for i, r in df.iterrows())


def get_weights(graph_name, graph):
    x = np.load(f"test_graphs/{graph_name}_weights.npz")

    normal = graph.new_ep("double")
    normal.a = x["normal"]

    uniform = graph.new_ep("double")
    uniform.a = x["uniform"]

    unit = graph.new_ep("double")
    unit.a = np.ones(graph.num_edges())

    return normal, uniform, unit


def save_true_value(graph_name, weights, result):
    if not RESULTS_FILE_TRUE_VALUES.exists():
        RESULTS_FILE_TRUE_VALUES.write_text(";".join(
            ["Name", "Weights", "True value"]
        ) + "\n")

    with RESULTS_FILE_TRUE_VALUES.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            graph_name, weights, result
        ]) + "\n")


if __name__ == "__main__":
    args = set(sys.argv)

    weighted = "--weighted" in args
    run_uniform = "--uniform" in args
    run_unit = "--unit" in args

    method = "ru"

    if "--sample-rp" in args:
        method = "rp"
    elif "--sample-ou" in args:
        method = "ou"

    true_values = "--true-values" in args

    if run_uniform and run_unit:
        print("Imcompatible")
        sys.exit(1)

    output = {}

    graphs_to_test = [
        "as-22july06",
        "cond-mat-2003",
        Path("test_graphs/ba-graph.gt"),
        Path("test_graphs/er-graph.gt"),
    ]

    if true_values:
        for graph_name in tqdm(graphs_to_test):
            graph_name, graph = get_graph_for_table(graph_name)

            result = springbok.average_path_length(graph)

            save_true_value(graph_name, None, result)

            normal, uniform, unit = get_weights(graph_name, graph)

            for n, w in [("normal", normal), ("uniform", uniform), ("unit", unit)]:
                result = springbok.average_path_length(graph, weights=w)

                save_true_value(graph_name, n, result)

    elif not weighted:

        for graph_name in tqdm(graphs_to_test):

            graph_name, graph = get_graph_for_table(graph_name)

            runs = target_runs["unweighted"]
            already_processed = get_already_processed()

            for run in trange(runs, leave=False, desc=graph_name):

                for sample in tqdm(sampling_sizes, leave=False):
                    debug_information = {}

                    if already_processed.get((graph_name,
                                              method,
                                              str(sample)), 0) > run:
                        continue

                    with measure(output=output):
                        result = springbok.average_path_length(graph,
                                                               sample=sample,
                                                               debug_information=debug_information,
                                                               sample_method=method)

                    save_result(graph_name, run, sample, result, debug_information, output, method)

    else:
        for graph_name in tqdm(graphs_to_test):
            graph_name, graph = get_graph_for_table(graph_name)

            runs = target_runs["weighted"]["full" if method == "ou" else "reduced"]

            normal, uniform, unit = get_weights(graph_name, graph)

            weights = normal
            tp = "normal"
            if run_uniform:
                weights = uniform
                tp = "uniform"
            if run_unit:
                weights = unit
                tp = "unit"
            tp += f"_{method}"

            already_processed = get_already_processed_weighted()

            _, lcc = springbok.average_path_length(graph, weights=weights, return_lcc=True)

            ss = sampling_sizes
            if graph_name == "er-graph":
                ss = [decimal.Decimal(x) for x in sampling_sizes_reduced_str]

            for run in trange(runs, leave=False, desc=graph_name):

                for sample in tqdm(ss, leave=False):
                    if already_processed.get((graph_name, tp, str(sample)), 0) > run:
                        continue

                    debug_information = {}
                    with measure(output=output):
                        result = springbok.average_path_length(graph,
                                                               sample=sample,
                                                               debug_information=debug_information,
                                                               weights=weights,
                                                               lcc=lcc,
                                                               sample_method=method)

                    save_result_weighted(graph_name, run, sample, result, debug_information, output,
                                         tp=tp)
