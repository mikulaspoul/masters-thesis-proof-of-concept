"""
This file tests returning and using of the distance map on the reduced graph
"""
import springbok
from sampling_run_experiments import get_weights
from springbok.utils import measure
from table_utils import get_graph_for_table

graph_name, graph = get_graph_for_table("as-22july06")
normal, uniform = get_weights(graph_name, graph)


with measure("eh", silent=False):
    a_normal, normal_lcc = springbok.average_path_length(graph, weights=normal, return_lcc=True)
    a_uniform, uniform_lcc = springbok.average_path_length(graph, weights=uniform, return_lcc=True)

print(a_normal, a_uniform)

with measure("eh", silent=False):
    a_normal = springbok.average_path_length(graph, weights=normal, lcc=normal_lcc)
    a_uniform = springbok.average_path_length(graph, weights=uniform, lcc=uniform_lcc)

print(a_normal, a_uniform)

with measure("eh s", silent=False):
    a_normal = springbok.average_path_length(graph, weights=normal, lcc=normal_lcc, sample=0.02)
    a_uniform = springbok.average_path_length(graph, weights=uniform, lcc=uniform_lcc, sample=0.02)

print(a_normal, a_uniform)
