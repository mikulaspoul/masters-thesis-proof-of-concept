from resource import getrusage, RUSAGE_SELF
from time import time as timestamp
from contextlib import contextmanager


@contextmanager
def measure(desc=None, output=None, silent=True):
    """ Measures the time for running the block, either printing the result of putting it in the `output`.
    """
    if silent and output is None:
        yield
        return

    start_time, start_resources = timestamp(), getrusage(RUSAGE_SELF)
    yield
    end_resources, end_time = getrusage(RUSAGE_SELF), timestamp()

    ro = {'real': end_time - start_time,
          'sys': end_resources.ru_stime - start_resources.ru_stime,
          'user': end_resources.ru_utime - start_resources.ru_utime}

    r = {k: "{:09.5f}".format(v, 5) for k, v in ro.items()}

    if not silent:
        if desc is None:
            print(r)
        else:
            print(desc.ljust(10), ":", r)

    if output is not None and isinstance(output, dict):
        output.update(ro)
