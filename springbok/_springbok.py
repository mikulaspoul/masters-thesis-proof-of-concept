import decimal
import itertools
from collections import defaultdict
from typing import Optional

import graph_tool as gt
import numpy as np
from graph_tool import topology, stats

from springbok._dist import shortest_distance
from . import libspringbok
from .utils import measure

WEIGHTS_PROPERTY_NAME = "_springbok_weights"


def has_self_loops(graph):
    return not not stats.label_self_loops(graph).a.any()


def has_parallel_edges(graph):
    return not not stats.label_parallel_edges(graph).a.any()


def _check_graph(graph):
    if graph.is_directed():
        raise ValueError("Only works for undirected")
    if graph.is_reversed():
        raise ValueError("Only works for unreversed")
    if has_self_loops(graph):
        raise ValueError("Only works for graphs without self-loops")
    if has_parallel_edges(graph):
        raise ValueError("Only works for graphs without parallel edges")

    largest = topology.extract_largest_component(graph)

    if largest.num_vertices() < graph.num_vertices():
        raise ValueError("Not connected component")


def naive_average_path_length(graph: gt.Graph, weights: Optional[gt.EdgePropertyMap]=None, skip_checks=False):
    """ Returns the average path length within a graph using the naive algorithm.
    """
    if not skip_checks:
        _check_graph(graph)

    if weights is not None:
        weights_copy = weights.copy("double")
    else:
        weights_copy = None

    with measure("  sd", silent=True):
        dist_map = topology.shortest_distance(graph, weights=weights_copy)

    with measure("  sum", silent=True):
        average_path_length = libspringbok.naive_average_path_length(
            graph._Graph__graph,
            gt._prop("v", graph, dist_map),
        )

    return average_path_length


def _get_tree_distances(graph, onecore_nodes, onecore_tree_ids, use_weights):
    """
    Calculates the distances within the trees
    """
    trees = defaultdict(set)
    tree_property = graph.new_vertex_property("int")
    tree_property.a = np.zeros(graph.num_vertices())

    for node, tree in zip(onecore_nodes, onecore_tree_ids):
        trees[tree].add(node)
        tree_property[node] = tree + 1

    # run the appropriate algorithm within a tree if the size of the tree is > 2
    tree_distance_maps = {}
    for tree, tree_nodes in trees.items():
        if len(tree_nodes) > 2:
            view = gt.GraphView(graph, tree_property.a == tree + 1)
            view = gt.Graph(view, directed=False, prune=True)
            tree_distance_maps[int(tree)] = shortest_distance(
                view,
                weights=view.edge_properties[WEIGHTS_PROPERTY_NAME] if use_weights else None,
            )

    # create a list of flattened numpy arrays with the distances within a tree, ordered by tree id
    trees = {k: sorted(v) for k, v in trees.items()}
    tree_distances = []
    for tree in sorted(trees.keys()):
        tree_nodes = trees[tree]

        l = len(tree_nodes)

        if l == 1:
            tree_distances.append(np.array([0], dtype=float))
            continue
        elif l == 2:
            edge = graph.edge(tree_nodes[0], tree_nodes[1])
            if use_weights:
                distance = graph.edge_properties[WEIGHTS_PROPERTY_NAME][edge]
            else:
                distance = 1

            tree_distances.append(np.array([[0, distance], [distance, 0]], dtype=float).reshape(-1))
            continue

        tree = int(tree)
        within_tree = np.zeros((len(tree_nodes), len(tree_nodes)), dtype=float)

        d = tree_distance_maps[tree]

        for i, node1 in enumerate(tree_nodes):
            within_tree[i, :] = d[i].a

        within_tree = within_tree.reshape(-1)  # flatten

        tree_distances.append(within_tree)

    return tree_distances


def average_path_length(
        graph: gt.Graph,
        weights: Optional[gt.EdgePropertyMap]=None,
        skip_checks=False,
        debug_information=None,
        sample=None,
        sample_method="ru",
        return_lcc=False,
        lcc=None,
):
    """
    Calculates the average path length on the graph.

    :param graph: The graph.
    :param weights: If provided, uses 2-chains as well.
    :param skip_checks: Skips checks on algorithm compatibility
    :param debug_information: If a dictionary is passed, some debug information are set
    :param sample: Approximate the value, with this sample size (float/Decimal 0-1
         as a portion of nodes to use or int with specific number)
    :param sample_method: What method to use in sampling (ru/rp/ou)
    :param return_lcc: Return the pairwise distances in the reduced graph
    :param lcc: The pairwise distances in the reduced graph, if passed it is not calculated again.
    :return:
    """
    if not skip_checks:
        _check_graph(graph)

    sample_probabilities = None
    sample_on_reduced = True

    if sample is not None:
        sample_method = sample_method.lower()
        if sample_method == "ru":
            sample_probabilities = None
            sample_on_reduced = True
        elif sample_method == "rp":
            sample_probabilities = 1
            sample_on_reduced = True
        elif sample_method == "ou":
            sample_probabilities = None
            sample_on_reduced = False
        else:
            raise ValueError("Invalid sample method")

    if debug_information is None:
        debug_information = {}

    debug_information["original_nodes"] = graph.num_vertices()
    debug_information["original_edges"] = graph.num_edges()

    use_weights = weights is not None
    if use_weights:
        if (weights.a <= 0).any():
            raise ValueError("Only works for positive weights")
        graph.properties[("e", WEIGHTS_PROPERTY_NAME)] = weights.copy("double")

    # filter the 1core
    with measure("  1core", silent=True):
        onecore_filter_nodes = graph.new_vertex_property("bool")
        onecore_filter_results = libspringbok.filter_1core(
            graph._Graph__graph,
            gt._prop("v", graph, onecore_filter_nodes),
            gt._prop("e", graph, graph.ep[WEIGHTS_PROPERTY_NAME] if use_weights else graph.new_edge_property("double")),
            use_weights,
        )

    onecore_nodes, onecore_parents, onecore_distances, onecore_tree_ids = onecore_filter_results

    # get distances within the trees
    with measure("  td", silent=True):
        trees_distances = _get_tree_distances(graph, onecore_nodes, onecore_tree_ids, use_weights=use_weights)

    debug_information["1core_size"] = len(onecore_nodes)

    # filter the 2chains, if weightes provided
    with measure("  2chains", silent=True):
        twochain_filter_nodes = graph.new_vertex_property("bool")

        if use_weights:
            twochain_filter_results = libspringbok.filter_2chain(
                gt.GraphView(graph, onecore_filter_nodes.a == False)._Graph__graph,
                gt._prop("v", graph, twochain_filter_nodes),
                gt._prop("e", graph, graph.ep[WEIGHTS_PROPERTY_NAME]),
            )
        else:
            twochain_filter_results = libspringbok.filter_2chain_dummy()

    (twochain_nodes, twochain_ids,
     twochain_parents_left, twochain_parents_right,
     twochain_distances_left, twochain_distances_right,
     twochain_distance) = twochain_filter_results

    twochain_size = len(twochain_nodes)
    debug_information["2chain_size"] = twochain_size

    # create a reduced graph without 1-core and 2-chains
    with measure("  reduce", silent=True):
        if use_weights and twochain_size:
            filter_nodes = onecore_filter_nodes.a | twochain_filter_nodes.a
            view = gt.GraphView(graph, filter_nodes == False)
        else:
            view = gt.GraphView(graph, onecore_filter_nodes.a == False)

        vorder = view.new_vertex_property("int")

        original_number_dict = {}
        for i, x in enumerate(view.vertices()):
            vorder[x] = i
            original_number_dict[i] = int(x)

        view = gt.Graph(view, directed=False, prune=True, vorder=vorder)

        original_number = view.new_vertex_property("int")
        for k, v in original_number_dict.items():
            original_number[k] = v

        if use_weights and twochain_size:
            new_edges = []
            update_weights = []

            chain_distances = defaultdict(lambda: float("inf"))

            for (l, r, v) in zip(twochain_parents_left, twochain_parents_right, twochain_distance):
                chain_distances[(l, r)] = min(chain_distances[(l, r)], v)

            for (l, r), v in chain_distances.items():
                new_edges.append((vorder[l], vorder[r]))
                update_weights.append(v)

            view.add_edge_list(new_edges)

            view.edge_properties[WEIGHTS_PROPERTY_NAME].a[-len(update_weights):] = update_weights

    debug_information["reduced_nodes"] = view.num_vertices()
    debug_information["reduced_edges"] = view.num_edges()

    # select vertices for sampling
    sample_from_original = sample_from = None
    if sample is not None:
        if not isinstance(sample, int):
            sample = decimal.Decimal(sample)
            sample = sample * (view if sample_on_reduced else graph).num_vertices()
            sample = int(sample.to_integral_exact(rounding=decimal.ROUND_CEILING))

        if sample <= 0:
            raise ValueError("Sample size must be positive")

        if sample_on_reduced:
            debug_information["sampled_nodes"] = sample

            if sample >= view.num_vertices():
                sample_from = None
            else:
                choices = np.arange(view.num_vertices())
                if sample_probabilities:
                    if 0 > sample_probabilities:
                        raise ValueError("Sample probabilities < 0")

                    s = sample_probabilities
                    p = np.ones(view.num_vertices())

                    tree_size = defaultdict(int)
                    for parent in onecore_filter_results[1]:
                        tree_size[parent] += 1

                    chain_nodes = defaultdict(float)
                    for n, pl, pr in zip(twochain_nodes, twochain_parents_left, twochain_parents_right):
                        tree_from_node_size = tree_size.pop(n) if n in tree_size else 0
                        chain_nodes[pl] += s / 2 + s * tree_from_node_size / 2
                        chain_nodes[pr] += s / 2 + s * tree_from_node_size / 2

                    for n, extra in itertools.chain(tree_size.items(), chain_nodes.items()):
                        p[vorder[n]] += s *     extra

                    p = p / p.sum()
                else:
                    p = None

                sample_from = np.random.choice(choices, sample, replace=False, p=p)

        else:
            sample_from_original = np.random.choice(np.arange(graph.num_vertices()), sample, replace=False)

            sample_from_reduced = set()
            twochain_nodes_dict = {k: i for i, k in enumerate(twochain_nodes)}
            onecore_nodes_dict = {k: i for i, k in enumerate(onecore_nodes)}

            for n in sample_from_original:
                if n in twochain_nodes_dict:  # add both parents of 2chain nodes
                    i = twochain_nodes_dict[n]
                    sample_from_reduced.add(twochain_parents_right[i])
                    sample_from_reduced.add(twochain_parents_left[i])
                elif n in onecore_nodes_dict:  # add either parent of 1core node or if that node is 2chain, add parents
                    i = onecore_nodes_dict[n]
                    p = onecore_filter_results[1][i]
                    if p in twochain_nodes_dict:
                        i = twochain_nodes_dict[p]
                        sample_from_reduced.add(twochain_parents_right[i])
                        sample_from_reduced.add(twochain_parents_left[i])
                    else:
                        sample_from_reduced.add(p)
                else:  # remaining node
                    sample_from_reduced.add(n)

            sample_from = np.empty(len(sample_from_reduced))

            for i, n in enumerate(sample_from_reduced):
                sample_from[i] = vorder[n]

            debug_information["sampled_nodes"] = len(sample_from_reduced)

    # calculate distances within the remaining graph
    with measure("  lscc", silent=True):
        if lcc is None:
            dist_map = shortest_distance(view,
                                         source=sample_from,
                                         weights=view.edge_properties[WEIGHTS_PROPERTY_NAME] if use_weights else None)
        else:
            dist_map = view.new_vertex_property(lcc.value_type())

            if sample_from is not None:
                for x in sample_from:
                    dist_map[x] = lcc[x]
            else:
                for x in view.vertices():
                    dist_map[x] = lcc[x]

    if sample_from is None:
        sample_from_v = gt.Vector_size_t()
    else:
        sample_from_v = gt.Vector_size_t(len(sample_from))
        sample_from_v.a = sample_from

    if sample_from_original is None:
        sample_from_original_v = gt.Vector_size_t()
    else:
        sample_from_original_v = gt.Vector_size_t(len(sample_from_original))
        sample_from_original_v.a = sample_from_original

    # sum the distances and get the APL
    with measure("  sum", silent=True):
        average_path_length_result = libspringbok.average_path_length(
            view._Graph__graph,
            gt._prop("v", view, dist_map),
            gt._prop("v", view, original_number),
            *onecore_filter_results,
            trees_distances,

            use_weights,
            twochain_nodes,
            twochain_ids,
            twochain_parents_left,
            twochain_distances_left,
            twochain_parents_right,
            twochain_distances_right,

            sample_from_v,
            sample_from_original_v,
        )

    if use_weights:
        del graph.properties[("e", WEIGHTS_PROPERTY_NAME)]

    average_path_length = average_path_length_result[0]

    if sample_from is not None:
        debug_information["counted_nodes"] = average_path_length_result[1]

    if return_lcc:
        return average_path_length, dist_map

    return average_path_length
