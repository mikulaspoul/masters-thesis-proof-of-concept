/*
 * Implements the actual algorithms from the thesis - detecting of 1core and 2chains, the naive algorithm and the improved algorhtm.
 * See comments in individaul functions.
 */

#include "graph_tool.hh"
#include "graph_util.hh"
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/range/join.hpp>

#define REMAINING_MEMBER 0
#define ONECORE_MEMBER 1
#define TWOCHAIN_MEMBER 2

using namespace graph_tool;
using namespace boost;
using namespace std;

namespace py = boost::python;


inline double max2(int x, double y) {
    double x2 = (double) x;

    return max(x2, y);
}

inline double max2(double x, double y) {
    return max(x, y);
}

template<typename Graph, typename DistMap>
void naive_average_path_length(Graph &g, DistMap dist_map, double &result) {
    /*
     * Very basic code for the sum of all distances with enabled parallelisation.
     */

    typedef typename property_map<Graph, vertex_index_t>::type vertex_index_map_t;
    vertex_index_map_t vertex_index = get(vertex_index_t(), g);
    typedef typename vprop_map_t<double>::type::unchecked_t vmap_t;

    vmap_t sums(vertex_index, num_vertices(g));

    parallel_vertex_loop
            (g,
             [&](auto &v) {
                 double sum = 0;

                 for (auto x : dist_map[v]) {
                    sum += x;
                 }

                 sums[v] = sum;
             });

    double sum = 0;

    for (auto v : vertices_range(g)) {
//        std::cout << v << "\t" << sums[v] << std::endl;

        sum += sums[v];
    }

    result = sum / (num_vertices(g) * (num_vertices(g) - 1));
}


template<typename Graph, typename VertexMap, typename EdgeMap>
void filter_1core(
    const Graph &g,
    VertexMap filter_node,
    EdgeMap weights_map,
    bool use_weights,
    python::tuple &return_result
) {
    /*
     * Detects the 1-core on a graph and returns the info about it.
     * Described in detail in the thesis.
     */
    typedef typename boost::graph_traits<Graph>::vertex_descriptor v_t;
    typedef typename property_map<Graph, vertex_index_t>::type vertex_index_map_t;
    typedef typename vprop_map_t<int>::type::unchecked_t vmap_t;

    std::queue<v_t> to_process;
    std::set<v_t> onecore;
    std::vector<v_t> onecore_order;

    vertex_index_map_t vertex_index = get(vertex_index_t(), g);
    vmap_t degrees(vertex_index, num_vertices(g));

    std::map<v_t, v_t> parents;
    for (auto v  : vertices_range(g)) {
        degrees[v] = out_degree(v, g);

        if (degrees[v] == 1) {
            to_process.push(v);
        }
    }

    while (not to_process.empty()) {
        auto v = to_process.front();
        to_process.pop();

        onecore.insert(v);
        onecore_order.push_back(v);

        for (auto n : out_neighbors_range(v, g)) {
            if (onecore.find(n) != onecore.end()) {
                continue;
            }

            degrees[n] -= 1;
            parents[v] = n;

            if (degrees[n] == 1) {
                to_process.push(n);
            }
        }
    }

    std::map<v_t, double> distances;
    std::map<v_t, v_t> tree_id;

    for (auto v : boost::adaptors::reverse(onecore_order)) {
        auto parent = parents[v];
        double distance, distance_to_parent = 1;

        if (use_weights) {
            for (auto e : out_edges_range(v, g)) {
                auto t = target(e, g);

                if (t == parent) {
                    distance_to_parent = weights_map[e];
                    break;
                }
            }
        }

        if (onecore.count(parent)) {
            distance = distance_to_parent + distances[parent];
            tree_id[v] = tree_id[parent];
        } else {
            tree_id[v] = v;
            distance = distance_to_parent;
        }

        distances[v] = distance;
    }

    std::vector<double> distances_v; distances_v.reserve(onecore_order.size());
    std::vector<v_t> parents_v; parents_v.reserve(onecore_order.size());
    std::vector<v_t> tree_id_v; tree_id_v.reserve(onecore_order.size());

    for (auto v : onecore_order) {
        distances_v.push_back(distances[v]);
        parents_v.push_back(parents[tree_id[v]]);
        tree_id_v.push_back(tree_id[v]);
        filter_node[v] = true;
    }

    return_result = boost::python::make_tuple(onecore_order, parents_v, distances_v, tree_id_v);
}

inline int xy(int dim, int x, int y) {
    // For accessing elements in a 2D numpy array converted into a single 1D vector
    return x * dim + y;
}

template<typename Graph, typename v_t>
void _traverse_2chain(
        const Graph &g,
        v_t vertex,
        bool use_min,
        std::map<v_t, v_t> & parents,
        bool set_direction,
        v_t & direction,
        v_t & parent,
        vector<v_t> & route
) {
    /*
     * Traverses a chain in one direction, returning what route it took, if not already processed in a different traversal,
     */
    if (parents.find(vertex) != parents.end()) {
        return;
    }

    parent = vertex;
    v_t previous;

    while (out_degree(parent, g) == 2) {
        v_t next = 0;
        bool next_set = false;

        for (auto candidate : out_neighbors_range(parent, g)) {
            if (candidate == previous) continue;

            if (!next_set) {
                next = candidate;
                next_set = true;
            } else {
                next = use_min ? std::min(next, candidate) : std::max(next, candidate);
            }
        }

        previous = parent;
        route.push_back(previous);
        parent = next;

        if (set_direction) {
            direction = next;
            set_direction = false;
        }
    }
}

template<typename Graph, typename v_t>
typename boost::graph_traits<Graph>::edge_descriptor get_edge(const Graph &g, v_t s, v_t t) {
    // gets edge from one vertex to another
    for (auto e : out_edges_range(s, g)) {
        if (target(e, g) == vertex(t, g)) {
            return e;
        }
    }

    throw false;
}

template<typename Graph, typename EdgeMap, typename v_t>
void _get_2chain_distances(
        const Graph &g,
        EdgeMap weights_map,
        std::vector<std::pair<std::pair<v_t, v_t>, std::set<v_t>>> & twochains,
        std::map<v_t, double> & distances_left,
        std::map<v_t, double> & distances_right,
        std::map<v_t, double> & twochain_distance
) {
    /*
     * Traverses a 2chain from one side to the other and back calculating the distances.
     */
    typedef typename boost::graph_traits<Graph>::edge_descriptor e_t;

    for (auto chain : twochains) {
        auto chain_left = chain.first.first, chain_right = chain.first.second;
        auto & chain_members  = chain.second;

        double distance = 0;
        vector<double> distances; distances.reserve(chain_members.size());
        vector<v_t> order; order.reserve(chain_members.size());

        auto previous = chain_left;

        v_t node;
        e_t edge;

        for (auto x : chain_members) {
            try {
                edge = get_edge(g, x, previous);
            } catch (...) {
                continue;
            }
            node = source(edge, g);
        }

        while (node != chain_right) {
            order.push_back(node);
            distance += weights_map[edge];
            distances.push_back(distance);
            distances_left[node] = distance;


            for (auto e : out_edges_range(node, g)) {
                if (target(e, g) == previous) continue;

                edge = e;
                break;
            }
            previous = node;
            node = target(edge, g);
        }

        distance += weights_map[edge];

        size_t i = distances.size() - 1;

        for (auto node : boost::adaptors::reverse(order)) {
            distances_right[node] = distance - distances[i--];
            twochain_distance[node] = distance;
        }
    }
}

template<typename Graph, typename VertexMap, typename EdgeMap>
void filter_2chain(
        const Graph &g,
        VertexMap filter_node,
        EdgeMap weights_map,
        python::tuple &return_result
) {
    /*
     * Detects the twochain, calculates the distances and returns all info.
     * See more info in the thesis.
     */
    typedef typename boost::graph_traits<Graph>::vertex_descriptor v_t;

    std::set<v_t> twochain;
    std::map<v_t, v_t> parents_left, parents_right, twochain_ids;
    std::map<v_t, double> distances_left, distances_right, twochain_distance;

    for (auto v : vertices_range(g)) {
        if (out_degree(v, g) == 2) {
            twochain.insert(v);
        }
    }

    std::vector<std::pair<std::pair<v_t, v_t>, std::set<v_t>>> twochains;

    for (auto v : twochain) {
        v_t direction, parent_left, parent_right;
        vector<v_t> route1, route2;

        _traverse_2chain(g, v, true, parents_left, true, direction, parent_left, route1);
        _traverse_2chain(g, v, false, parents_right, false, direction, parent_right, route2);

        set<v_t> s;

        if (route1.size() and route2.size()) {
            for (auto x : boost::join(route1, route2)) {
                parents_left[x] = parent_left;
                parents_right[x] = parent_right;
                s.insert(x);
            }

            twochains.push_back(std::make_pair(std::make_pair(parent_left, parent_right), s));
        }
    }

    _get_2chain_distances(g, weights_map, twochains, distances_left, distances_right, twochain_distance);

    uint64_t i = 0;
    for (auto chain : twochains) {
        for (auto x : chain.second) twochain_ids[x] = i;
        ++i;
    }

    vector<uint64_t> twochain_vector;
    vector<uint64_t> twochain_ids_vector;
    vector<uint64_t> twochain_parents_left_vector;
    vector<uint64_t> twochain_parents_right_vector;
    vector<double> twochain_distances_left_vector;
    vector<double> twochain_distances_right_vector;
    vector<double> twochain_distance_vector;

    for (auto v : twochain) twochain_vector.push_back(v);

    std::sort(twochain_vector.begin(), twochain_vector.end());

    for (auto v : twochain_vector) {
        twochain_ids_vector.push_back(twochain_ids[v]);
        twochain_parents_left_vector.push_back(parents_left[v]);
        twochain_parents_right_vector.push_back(parents_right[v]);
        twochain_distances_left_vector.push_back(distances_left[v]);
        twochain_distances_right_vector.push_back(distances_right[v]);
        twochain_distance_vector.push_back(twochain_distance[v]);
        filter_node[v] = true;
    }

    return_result = boost::python::make_tuple(
            twochain_vector,
            twochain_ids_vector,
            twochain_parents_left_vector,
            twochain_parents_right_vector,
            twochain_distances_left_vector,
            twochain_distances_right_vector,
            twochain_distance_vector
    );
}

void _get_orders(
        map<uint64_t, vector<uint64_t>> & tree_members,
        vector<int> & tree_order,
        vector<int> & order_in_tree
        ) {
    /*
     * Orders trees by ID.
     */
    vector<uint64_t> tree_order_help(tree_members.size());

    int i = 0;
    for (auto & s : tree_members) {
        auto k = s.first;
        auto & v = s.second;
        tree_order_help[i++] = k;

        std::sort(v.begin(), v.end());

        for (size_t j = 0; j < v.size(); ++j) {
            order_in_tree[v[j]] = (int) j;
        }
    }

    std::sort(tree_order_help.begin(), tree_order_help.end());

    for (uint64_t i = 0; i < tree_order_help.size(); ++i) {
        tree_order[tree_order_help[i]] = i;
    }
}

double get_sum_for_onecore_node(
    size_t total_vertices,
    uint64_t child, uint64_t ti, uint64_t to, uint64_t ts,
    double distance_to_parent, double parent_sum,

    const vector<double> & tree_sums,
    const map<uint64_t, vector<uint64_t>> & tree_members,
    const vector<vector<double>> & onecore_tree_distances,
    const vector<int> & order_in_tree
) {
    /*
     * Sums distances from a vertex on a 1-tree to all other vertices.
     */
    double ls;

    ls = parent_sum;  // distance to all nodes through the parent node
    ls -= tree_sums[to];  // without distances to the tree of the child
    // plus distance to the parents times nodes excluding this tree
    ls += distance_to_parent * (total_vertices - ts);

    for (auto w: tree_members.find(ti)->second) {
        auto dwt = onecore_tree_distances[to][xy(ts, order_in_tree[w], order_in_tree[child])];

        ls += dwt;
    }

    return ls;
}

inline void sum_tree_children(
    const vector<uint64_t> & children,
    const map<uint64_t, size_t> & onecore_order,
    const vector<double> & onecore_distances,
    const vector<uint64_t> & onecore_tree_ids,
    const vector<int> & tree_order,
    double x,
    double & ls,
    vector<double> & tree_sums

) {
    /*
     * Sums the distances from a vertex to vertices on 1-core trees related to it.
     */
    for (auto tree_child: children) {
        auto tree_child_order = onecore_order.find(tree_child)->second;
        auto y = x + onecore_distances[tree_child_order];

        ls += y;

        auto to = tree_order[onecore_tree_ids[tree_child_order]];
        tree_sums[to] += y;
    }
}

template<typename Graph, typename DistMap>
void average_path_length(
        Graph &g,
        DistMap dist_map,
        boost::any original_order_any,

        // 1core
        const vector<uint64_t> &onecore,  // list of nodes in the 1core
        const vector<uint64_t> &onecore_parents,  // the parents of the nodes in the order being listed in onecore
        const vector<uint64_t> &onecore_tree_ids,  // same order, the tree id of the node
        const vector<double> &onecore_distances,  // same order, the distance to the parent
        const vector<vector<double>> & onecore_tree_distances,

        // 2chain
        bool use_twochain,
        const vector<uint64_t> &twochain, // list of nodes in 2chain
        const vector<uint64_t> &twochain_ids,
        const vector<uint64_t> &twochain_parents_left,
        const vector<double> &twochain_distances_left,
        const vector<uint64_t> &twochain_parents_right,
        const vector<double> &twochain_distances_right,

        const vector<uint64_t> &sample_from,
        const vector<uint64_t> &sample_only_count_vertices,

        py::tuple &result
) {
    /*
     * Handles the sum of the distances with both 1-core and 2-chain simplifications.
     * A combination of the two algorithms for summing in the thesis.
     */
    auto onecore_size = onecore.size();
    auto twochain_size = twochain.size();
    auto remaining_vertices = num_vertices(g);
    auto total_vertices = onecore_size + twochain_size + remaining_vertices;

    typedef typename vprop_map_t<int32_t>::type vprop_t;
    vprop_t original_order = boost::any_cast<vprop_t>(original_order_any);

    // INITAL PROCESSING into helper maps and sets

    vector<double> sums(total_vertices, 0);
    vector<int> node_type(total_vertices, REMAINING_MEMBER);

    vector<uint64_t> reverse_original_order(total_vertices);
    for (auto v: vertices_range(g)) {
        reverse_original_order[original_order[v]] = v;
    }

    bool use_sampling = sample_from.size() > 0;
    const set<uint64_t> sample_from_set(sample_from.begin(), sample_from.end());
    set<uint64_t> sample_from_total;

    if (use_sampling) {
        for (auto v : sample_from) {
            sample_from_total.insert(original_order[v]);
        }
    }

    map<uint64_t, size_t> onecore_order;
    map<uint64_t, vector<uint64_t>> children;
    map<uint64_t, vector<uint64_t>> tree_members;

    for (size_t i = 0; i < onecore_size; ++i) {
        auto p = onecore_parents[i];
        auto w = onecore[i];
        auto t = onecore_tree_ids[i];

        onecore_order[w] = i;
        children[p].push_back(w);
        tree_members[t].push_back(w);
        node_type[w] = ONECORE_MEMBER;
    }

    vector<uint64_t> twochain_reverse_order(total_vertices);

    for (size_t i = 0; i < twochain_size; ++i) {
        auto w = twochain[i];
        node_type[w] = TWOCHAIN_MEMBER;
        twochain_reverse_order[w] = i;

        auto p_l = twochain_parents_left[i];
        auto p_r = twochain_parents_right[i];

        if (use_sampling) {
            if (sample_from_total.find(p_l) != sample_from_total.end() and sample_from_total.find(p_r) != sample_from_total.end()) {
                sample_from_total.insert(w);
            }
        }
    }

    if (use_sampling) {
        for (size_t i = 0; i < onecore_size; ++i) {
            auto p = onecore_parents[i];
            auto w = onecore[i];

            if (sample_from_total.find(p) != sample_from_total.end()) {
                sample_from_total.insert(w);
            }
        }
    }

    vector<int> tree_order(total_vertices);
    vector<int> order_in_tree(total_vertices);

    _get_orders(tree_members, tree_order, order_in_tree);

    int num_trees = (int) tree_members.size();

    // SUM from remaining vertices to all other
    parallel_vertex_loop(g, [&](auto &node) { // iterates only over the non-filtered nodes
        if (use_sampling) {
            if (sample_from_set.find(node) == sample_from_set.end()) {
                return;
            }
        }

        double remaining_sum = 0;
        double trees_sum = 0;
        double twochains_sum = 0;

        for (auto w: vertices_range(g)) {
            remaining_sum += dist_map[node][w];
        }

        vector<double> twochain_distances_to_node(twochain_size);

        for (size_t i = 0; i < twochain_size; ++i) {
            auto p_l = reverse_original_order[twochain_parents_left[i]];
            auto p_r = reverse_original_order[twochain_parents_right[i]];

            auto d_l = dist_map[node][p_l] + twochain_distances_left[i];
            auto d_r = dist_map[node][p_r] + twochain_distances_right[i];

            auto x = min(d_l, d_r);

            twochain_distances_to_node[i] = x;

            twochains_sum += x;
        }

        vector<double> tree_sums(num_trees, 0);

        for (size_t i = 0; i < onecore_size; ++i) {
            auto parent_original_order = onecore_parents[i];
            auto parent = reverse_original_order[parent_original_order];

            double distance_from_parent;

            if (use_twochain and node_type[parent_original_order] == TWOCHAIN_MEMBER) {
                distance_from_parent = twochain_distances_to_node[twochain_reverse_order[parent_original_order]];
            } else {
                distance_from_parent = dist_map[node][parent];
            }

            auto x = distance_from_parent + onecore_distances[i];
            auto to = tree_order[onecore_tree_ids[i]];

            tree_sums[to] += x;

            trees_sum += x;
        }

        auto original_node_order = original_order[node];

        sums[original_node_order] = remaining_sum + trees_sum + twochains_sum;

        auto x = children.find(original_node_order);

        if (x == children.end()) {
            return;
        }

        for (auto child : x->second) {
            auto child_order = onecore_order.find(child)->second;
            auto ti = onecore_tree_ids[child_order];
            auto to = tree_order[ti];
            auto ts = tree_members.find(ti)->second.size();
            auto distance_to_parent = onecore_distances[child_order];

            auto x = get_sum_for_onecore_node(
                total_vertices,
                child, ti, to, ts,
                distance_to_parent, sums[original_node_order],

                tree_sums,
                tree_members,
                onecore_tree_distances,
                order_in_tree
            );

            sums[child] = x;
        }
    });

    // SUM from twochain vertices to all other
    #pragma omp parallel for schedule(runtime) if (twochain_size > 300)
    for (size_t process_i = 0; process_i < twochain_size; ++process_i) {
        auto w = twochain[process_i];

        if (use_sampling and sample_from_total.find(w) == sample_from_total.end()) {
            continue;
        }

        vector<double> tree_sums(num_trees, 0);

        double ls = 0;

        auto p_l = reverse_original_order[twochain_parents_left[process_i]];
        auto p_r = reverse_original_order[twochain_parents_right[process_i]];

        for (auto v  : vertices_range(g)) {
            auto d_l = dist_map[p_l][v] + twochain_distances_left[process_i];
            auto d_r = dist_map[p_r][v] + twochain_distances_right[process_i];
            auto x = min(d_l, d_r);

            ls += x;

            auto children_to_process = children.find(original_order[v]);

            if (children_to_process != children.end()) {
                sum_tree_children(
                        children_to_process->second, onecore_order, onecore_distances, onecore_tree_ids, tree_order,
                        x,
                        ls, tree_sums
                );
            }
        }
        for (size_t other_i = 0; other_i < twochain_size; ++other_i) {
            double x;

            if (process_i == other_i) {
                x = 0;
            } else {
                auto o_p_l = reverse_original_order[twochain_parents_left[other_i]];
                auto o_p_r = reverse_original_order[twochain_parents_right[other_i]];

                x = min(
                        min(dist_map[p_l][o_p_l] + twochain_distances_left[process_i] + twochain_distances_left[other_i],
                            dist_map[p_l][o_p_r] + twochain_distances_left[process_i] + twochain_distances_right[other_i]),
                        min(dist_map[p_r][o_p_l] + twochain_distances_right[process_i] + twochain_distances_left[other_i],
                            dist_map[p_r][o_p_r] + twochain_distances_right[process_i] + twochain_distances_right[other_i])
                );

                if (twochain_ids[process_i] == twochain_ids[other_i]) {
                    x = min(x, abs(twochain_distances_left[process_i] - twochain_distances_left[other_i]));
                }
            }

            ls += x;

            auto children_to_process = children.find(twochain[other_i]);

            if (children_to_process != children.end()) {
                sum_tree_children(
                        children_to_process->second, onecore_order, onecore_distances, onecore_tree_ids,
                        tree_order,
                        x,
                        ls, tree_sums
                );
            }
        }

        sums[w] = ls;

        auto x = children.find(w);

        if (x == children.end()) {
            continue;
        }

        for (auto child : x->second) {
            auto child_order = onecore_order.find(child)->second;
            auto ti = onecore_tree_ids[child_order];
            auto to = tree_order[ti];
            auto ts = tree_members.find(ti)->second.size();
            auto distance_to_parent = onecore_distances[child_order];

            auto x = get_sum_for_onecore_node(
                    total_vertices,
                    child, ti, to, ts,
                    distance_to_parent, ls,

                    tree_sums,

                    tree_members,
                    onecore_tree_distances,
                    order_in_tree
            );

            sums[child] = x;
        }

    }

    // SUM of distances from each vertex altogether and the APL result
    double sum = 0;
    size_t counted_vertices;

    if (use_sampling) {
        if (sample_only_count_vertices.size()) {
            counted_vertices = sample_only_count_vertices.size();
            for (auto v : sample_only_count_vertices) {
                sum += sums[v];
            }
        } else {
            counted_vertices = sample_from_total.size();
            for (auto v :  sample_from_total) {
                sum += sums[v];
            }
        }
    } else {
        counted_vertices = total_vertices;
        for (uint64_t v = 0; v < total_vertices; ++v) {
//            std::cout << v << "\t" << sums[v] << std::endl;
            sum += sums[v];
        }
    }

//    std::cout << sum << " " << sample_from.size() << " " << counted_vertices << " " << total_vertices << " " << std::endl;

    double apl = sum / (counted_vertices * (total_vertices - 1));

    result = py::make_tuple(apl, counted_vertices);
}
