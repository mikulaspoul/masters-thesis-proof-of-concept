# Springbok

This is an [extension](https://graph-tool.skewed.de/static/doc/demos/cppextensions/cppextensions.html) to [graph-tool](https://graph-tool.skewed.de/), a graph library in Python.
The extension implements an improved algorithm for the average path length calculation on both weighted and unweighted graphs, but only undirected graphs are supported. 

The tool has to be compiled before usage, which can be done using the provided Makefile.
Just run the following snippet. As far as requirements go, graph-tool needs to be installed and the requirements for its compilation should be available (with exception of maybe cairo).
The extension was developed with graph-tool at version 2.28, but 2.29 should work fine.

```bash
make all
```

After that two functions are available in the `springbok` module, `naive_average_path_length` and `average_path_length`.
The first is a naive implementation of the calculation and has two arguments, one the `graph-tool` graph and the other for weights, which has to be an `EdgePropertyMap`, if `None` is passed, the graph is assumed to be unweighted.

Example usage:

```python
import graph_tool.all as gt
import springbok

graph = gt.random_graph(100, lambda: (3, 3))
print(springbok.naive_average_path_length(graph))
```

The latter is a improved implementation of the calculation as described in the [thesis](https://masters-thesis.mikulaspoul.cz/).
The two arguments are the same.
This function can also calculate an approximation, which can be done by passing the argument `sample`, which stands for the sample size, how big of a portion should be used for the sampling, a number between 0 and 1.
The default method (`sample_method`) is RU, which works fine for unweighted graphs, but for weighted graphs OU is recommended.
There are more options for the function, see the docstring in `_springbok.py`.

Example usage:


```python
import graph_tool.all as gt
import springbok

graph = gt.random_graph(100, lambda: (3, 3))
print(springbok.average_path_length(graph))

print(springbok.average_path_length(graph, sample=0.3))
```
