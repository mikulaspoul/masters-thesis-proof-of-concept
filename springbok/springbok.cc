/*
 * Defines the Boost Python module functions, a layer between `_springbok.py` and `springbok.hh`, where the implementaiton lives.
 * Mostly handles invoking the right function with the right template and some copying of memory.
 */

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include "springbok.hh"

namespace py = boost::python;
namespace np = boost::python::numpy;

double naive_average_path_length_bind(GraphInterface &gi, boost::any dist_map) {
    double result;

    gt_dispatch<>()(
        [&](auto& g, auto d){ naive_average_path_length(g, d, result); }, never_directed(), vertex_scalar_vector_properties()
    )(
        gi.get_graph_view(), dist_map
    );

    return result;
}

boost::python::tuple filter_1core_bind(
        GraphInterface& gi,
        boost::any filter_node_property,
        boost::any weights_property,
        bool use_weights
) {
    boost::python::tuple result;

    gt_dispatch<>()(
            [&](auto& g, auto d, auto e){ filter_1core(g, d, e, use_weights, result); },
            never_directed(), writable_vertex_scalar_properties(), edge_floating_properties()
    )(
            gi.get_graph_view(), filter_node_property, weights_property
    );

    return result;
}

boost::python::tuple filter_2chain_bind(
        GraphInterface& gi,
        boost::any filter_node_property,
        boost::any weights_property
) {
    boost::python::tuple result;

    gt_dispatch<>()(
            [&](auto& g, auto d, auto e){ filter_2chain(g, d, e, result); },
            never_directed(), writable_vertex_scalar_properties(), edge_floating_properties()
    )(
            gi.get_graph_view(), filter_node_property, weights_property
    );

    return result;
}

boost::python::tuple filter_2chain_dummy_bind() {
    vector<uint64_t> twochain;
    vector<uint64_t> twochain_ids;
    vector<uint64_t> twochain_parents_left;
    vector<uint64_t> twochain_parents_right;
    vector<double> twochain_distances_left;
    vector<double> twochain_distances_right;
    vector<double> twochain_distance;

    return boost::python::make_tuple(
            twochain,
            twochain_ids,
            twochain_parents_left,
            twochain_parents_right,
            twochain_distances_left,
            twochain_distances_right,
            twochain_distance
    );
}

py::tuple average_path_length_bind(
        GraphInterface &gi,
        boost::any dist_map,
        boost::any original_number,

        vector<uint64_t> & onecore,
        vector<uint64_t> & parents,
        vector<double> & distances,
        vector<uint64_t> & tree_id,
        py::list & tree_distances,

        bool use_twochain,
        vector<uint64_t> & twochain,
        vector<uint64_t> & twochain_ids,
        vector<uint64_t> & twochain_parents_left,
        vector<double> & twochain_distances_left,
        vector<uint64_t> & twochain_parents_right,
        vector<double> & twochain_distances_right,

        vector<uint64_t> & sample_from,
        vector<uint64_t> & sample_only_count_vertices
) {
    py::tuple result;
    vector<vector<double>> tree_distances_v(py::len(tree_distances));

    // from a list of numpy arrays creates a vector of vectors
    np::dtype dt_double = np::dtype::get_builtin<double>();
    np::dtype dt_uint64_t = np::dtype::get_builtin<uint64_t>();

    for (int i = 0; i < py::len(tree_distances); ++i) {
        np::ndarray tree_distance = py::extract<np::ndarray>(tree_distances[i]);
        tree_distance = tree_distance.astype(dt_double);

        int size = tree_distance.shape(0);

        double* ptr = reinterpret_cast<double*>(tree_distance.get_data());
        tree_distances_v[i].assign(ptr, ptr + size);
    }

    gt_dispatch<>()(
            [&](auto &g, auto d) {
                average_path_length(g, d, original_number,

                        // 1core
                        onecore, parents, tree_id, distances, tree_distances_v,
                        // 2chain
                        use_twochain, twochain, twochain_ids, twochain_parents_left, twochain_distances_left, twochain_parents_right, twochain_distances_right,

                        // sample
                        sample_from, sample_only_count_vertices,

                        result);
            }, never_directed(), vertex_scalar_vector_properties()
    )(
            gi.get_graph_view(), dist_map
    );

    return result;
}

BOOST_PYTHON_MODULE (libspringbok) {
    using namespace boost::python;

    boost::python::numpy::initialize();

    def("naive_average_path_length", &naive_average_path_length_bind);
    def("filter_1core", &filter_1core_bind);
    def("filter_2chain_dummy", &filter_2chain_dummy_bind);
    def("filter_2chain", &filter_2chain_bind);
    def("average_path_length", &average_path_length_bind);
}
