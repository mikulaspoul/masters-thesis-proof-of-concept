/*
 * This file implements functionality from `_dist.py`, distances within a graph from multiple (but not all) sources, both on weighted and unweighted graphs.
 * Supports parallelisation with OpenMP.
 * Only basic functionality.
 */

#include "graph.hh"
#include "graph_filtering.hh"
#include "graph_properties.hh"
#include "graph_selectors.hh"

#include <boost/python.hpp>

#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths_no_color_map.hpp>
#include <boost/graph/dag_shortest_paths.hpp>
#include <boost/python/stl_iterator.hpp>

using namespace std;
using namespace boost;
using namespace graph_tool;

struct stop_search {};

struct distances_from_sources_unweighted
{
    template <class DistMap, class PredMap>
    class bfs_visitor: public boost::bfs_visitor<null_visitor>
    {
    public:
        bfs_visitor(DistMap& dist_map, PredMap& pred, size_t source)
                : _dist_map(dist_map), _pred(pred), _source(source) {}

        template <class Graph>
        void initialize_vertex(typename graph_traits<Graph>::vertex_descriptor v, Graph&)
        {
            typedef typename DistMap::value_type dist_t;
            dist_t inf = std::is_floating_point<dist_t>::value ? numeric_limits<dist_t>::infinity() : numeric_limits<dist_t>::max();

            _dist_map[v] = (v == _source) ? 0 : inf;
            _pred[v] = v;
        }

        template <class Graph>
        void tree_edge(const typename graph_traits<Graph>::edge_descriptor& e, Graph& g) {
            _pred[target(e,g)] = source(e,g);
        }

        template <class Graph>
        void discover_vertex(typename graph_traits<Graph>::vertex_descriptor v, Graph&) {
            if (size_t(_pred[v]) == v)
                return;
            _dist_map[v] = _dist_map[_pred[v]] + 1;
        }

    private:
        DistMap& _dist_map;
        PredMap& _pred;
        size_t _source;
    };

    template <class Graph, class DistMap>
    void operator()(const Graph& g, DistMap dist_map, const std::vector<uint64_t> & sources) const
    {
        typedef typename graph_traits<Graph>::vertex_descriptor vertex_t;
        typedef typename property_traits<DistMap>::value_type dist_t;

        auto nv = num_vertices(g);

        #pragma omp parallel for schedule(runtime) if (sources.size() > OPENMP_MIN_THRESH)
        for (auto & v: sources) {
            dist_map[v].resize(nv, 0);
            vector<vertex_t> pred_map(nv);
            bfs_visitor<dist_t,vector<size_t>> vis(dist_map[v], pred_map, v);
            breadth_first_search(g, v, visitor(vis));
        }
    }
};

struct do_djk_search
{
    template <class Graph, class VertexIndexMap, class DistMap, class PredMap,
            class WeightMap>
    void operator()(const Graph& g, size_t source,
                    VertexIndexMap vertex_index, DistMap dist_map,
                    PredMap pred_map, WeightMap weight) const
    {
        typedef typename property_traits<DistMap>::value_type dist_t;

        constexpr dist_t inf = (std::is_floating_point<dist_t>::value) ? numeric_limits<dist_t>::infinity() : numeric_limits<dist_t>::max();

        dist_map[source] = 0;

        try {
            dijkstra_shortest_paths_no_color_map
                    (g, vertex(source, g), pred_map, dist_map, weight,
                     vertex_index, std::less<dist_t>(),
                     boost::closed_plus<dist_t>(), inf, dist_t(),
                     boost::dijkstra_visitor<null_visitor>());

        }
        catch (stop_search &) {}

    }
};


struct distances_from_sources_weighted
{

    template <class Graph, class DistMap, class WeightMap, class VertexIndexMap>
    void operator()(const Graph& g, DistMap dist_map, WeightMap weights, const std::vector<uint64_t> & sources,
                    VertexIndexMap vertex_index) const
    {
        typedef typename graph_traits<Graph>::vertex_descriptor vertex_t;
        typedef typename property_traits<DistMap>::value_type::value_type dist_t;

        typedef typename vprop_map_t<dist_t>::type vprop_t;
        typedef typename vprop_map_t<vertex_t>::type vertex_prop_t;

        auto nv = num_vertices(g);

        #pragma omp parallel for schedule(runtime) if (sources.size() > OPENMP_MIN_THRESH)
        for (auto & v: sources) {
            dist_map[v].resize(num_vertices(g), 0);

            vprop_t dist_map_v;

            auto dist_map_v2 = dist_map_v.get_unchecked(nv);

            vertex_prop_t pred_map;

            do_djk_search()(g, v, vertex_index, dist_map_v2, pred_map.get_unchecked(nv), weights);

            for (size_t i = 0; i < nv; ++i) {
                dist_map[v][i] = dist_map_v[i];
            }
        }
    }
};


void get_dists(GraphInterface& gi, std::vector<uint64_t> & sources,
               boost::any dist_map, boost::any weight)
{
    if (weight.empty()) {
        run_action<>()
                (gi, std::bind(distances_from_sources_unweighted(),
                               std::placeholders::_1, std::placeholders::_2, sources),
                 vertex_scalar_vector_properties())
                (dist_map);
    } else {
        run_action<>()
                (gi, std::bind(distances_from_sources_weighted(),
                               std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, sources,
                               gi.get_vertex_index()),
                 vertex_scalar_vector_properties(),
                 edge_scalar_properties())
                (dist_map, weight);
    }
}


BOOST_PYTHON_MODULE (libdist) {
    python::def("get_dists", &get_dists);
};
