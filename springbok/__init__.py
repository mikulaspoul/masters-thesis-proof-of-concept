from ._springbok import naive_average_path_length, average_path_length

__all__ = ["naive_average_path_length", "average_path_length"]
