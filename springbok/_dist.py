from typing import Optional, List, Union

import graph_tool as gt
import numpy as np

gt.dl_import("from graph_tool.topology import libgraph_tool_topology")
from . import libdist


def shortest_distance(g: gt.Graph,
                      source: Optional[Union[np.array, List]]=None,
                      weights: Optional[gt.EdgePropertyMap]=None) -> gt.VertexPropertyMap:
    """
    Returns the shortest distances within a graph from all vertices to all other vertices,
    or if `source` is provided, then just from those vertices to all other vertices.
    """
    if weights is None:
        dist_type = 'int32_t'
    else:
        dist_type = weights.value_type()

    dist_map = g.new_vertex_property(f"vector<{dist_type}>")

    if source is not None:
        source_v = gt.Vector_size_t(len(source))
        source_v.a = source

        libdist.get_dists(g._Graph__graph,
                          source_v,
                          gt._prop("v", g, dist_map),
                          gt._prop("e", g, weights))
    else:
        libgraph_tool_topology.get_all_dists(g._Graph__graph,
                                             gt._prop("v", g, dist_map),
                                             gt._prop("e", g, weights),
                                             False)  # dense is False

    return dist_map
