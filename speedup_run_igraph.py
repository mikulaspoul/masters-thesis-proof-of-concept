"""
This file runs the experiments to compare igraph's implementation with the naive graph-tool version.
"""
import sys
from collections import Counter
from pathlib import Path

import igraph as ig
import pandas as pd
from tqdm import tqdm, trange

from springbok.utils import measure

RESULTS_FILE_RAW = Path("outputs/igraph_speed_table_raw_runs.csv")


def save_result(name, run, avg_path_length, debug_information, measure_output):
    if not RESULTS_FILE_RAW.exists():
        RESULTS_FILE_RAW.write_text(";".join(
            ["Name", "Trial", "AVG distance", "Nodes", "Edges",
             "Real time", "User time", "Sys time",
             ]
        ) + "\n")

    with RESULTS_FILE_RAW.open("a") as fl:
        fl.write(";".join(str(x) for x in [
            name, run, avg_path_length,
            debug_information["original_nodes"],
            debug_information["original_edges"],
            measure_output["real"],
            measure_output["user"],
            measure_output["sys"],
        ]) + "\n")


def summarize_speedup_table():
    df_raw = pd.read_csv(str(RESULTS_FILE_RAW), delimiter=";")

    names = set(df_raw.Name)

    summarized_results = []

    for name in names:
        df_raw_name = df_raw[df_raw.Name == name]
        first = df_raw_name.iloc[0]

        results = {
            "Name": name
        }

        for x in ["AVG distance", "Nodes", "Edges"]:
            results[x] = first[x]

        results[f"AVG real"] = df_raw_name["Real time"].mean()
        results[f"AVG user"] = df_raw_name["User time"].mean()

        summarized_results.append(results)

    results = pd.DataFrame(summarized_results, columns=[
        'Name',
        'Nodes',
        'Edges',
        'AVG distance',
        'AVG real',
        'AVG user',

    ])

    results.sort_values("Name", inplace=True)

    results.to_csv("outputs/igraph_speed_table.csv", index=False)


def get_already_processed():
    try:
        df = pd.read_csv(RESULTS_FILE_RAW, delimiter=";")
    except Exception:
        return {}

    counter = Counter(df["Name"])
    already_processed = {}

    for k, v in counter.items():
        already_processed[k] = {
            "runs": v,
        }
    return already_processed


if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "--summarize":
        summarize_speedup_table()

        sys.exit(0)

    output = {}

    graphs_to_test = [
        ("er-graph", Path("test_graphs/er-graph.graphml")),
        ("ba-graph", Path("test_graphs/ba-graph.graphml")),
        ("as-22july06", Path("test_graphs/as-graph.graphml")),
        ("cond-mat-2003", Path("test_graphs/cond-graph.graphml")),
    ]

    already_processed = get_already_processed()

    for graph_name, graph_path in tqdm(graphs_to_test):

        graph: ig.Graph = ig.load(str(graph_path)).clusters().giant()

        already_processed_this_graph = already_processed.get(graph_name, {})
        already_ran = already_processed_this_graph.get("runs", 0)

        runs = 5

        for run in trange(already_ran, runs, leave=False, desc=graph_name):
            # init because ground truth doesn't do debug information
            debug_information = {"original_nodes": graph.vcount(), "original_edges": graph.ecount()}

            with measure(output=output):
                try:
                    result = graph.average_path_length()
                except RuntimeError:
                    print(f"skipping {graph_name}")
                    break
            save_result(graph_name, run, result, debug_information, output)
