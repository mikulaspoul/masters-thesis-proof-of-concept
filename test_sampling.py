"""
Tests sampling using methods RU and RP
"""
import numpy as np
from graph_tool import collection, topology
from pathlib import Path

import springbok
from springbok.utils import measure

graph = collection.data["power"]
graph = topology.extract_largest_component(graph, prune=True)

x = Path("test_graphs/power_weights.npz")
if x.exists():
    a = np.load(x)["uniform"]
else:
    a = np.random.uniform(0.001, 6, size=graph.num_edges())
    np.savez(x, uniform=a)

weights = graph.new_edge_property("float")
weights.a = a

print("NO WEIGHTS")
with measure("dumb", silent=False):
    avg = springbok.naive_average_path_length(graph)
print(avg)
print("")
with measure("clever", silent=False):
    avg = springbok.average_path_length(graph)
print(avg)
print("---------------\n\n")

print("WEIGHTS")
with measure("dumb", silent=False):
    avg = springbok.naive_average_path_length(graph, weights=weights)
print(avg)
print("")
with measure("clever", silent=False):
    avg = springbok.average_path_length(graph, weights=weights)
print(avg)

sample = 0.5
debug = {}
x = springbok.average_path_length(graph, sample=sample, debug_information=debug)
print(f"{sample:.2f}", f"{x:.5f}", debug.get("original_nodes"), debug.get("sampled_nodes"), debug.get("counted_nodes"))

x = springbok.average_path_length(graph, sample=sample, debug_information=debug, sample_method="ou")
print(f"{sample:.2f}", f"{x:.5f}", debug.get("original_nodes"), debug.get("sampled_nodes"), debug.get("counted_nodes"))


samples = np.array(list(np.arange(1, 21)) + list(np.arange(25, 101, 5))) / 100

print("SAMPLING NO WEIGHTS")

for sample in samples:
    print(f"{sample:.2f}", springbok.average_path_length(graph, sample=sample))


print("SAMPLING WEIGHTS")

for sample in samples:
    debug_information = {}
    x = springbok.average_path_length(graph, sample=sample, weights=weights, debug_information=debug_information)
    print(f"{sample:.2f}", f"{x:.5f}", debug_information.get("sampled_nodes"), debug_information.get("counted_nodes"))
