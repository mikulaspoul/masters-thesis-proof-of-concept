"""
This file contains some util functions for running experiments and putting results in tables.
"""
import graph_tool as gt
from graph_tool import topology, collection, stats


def get_graph_for_table(graph_name):
    if isinstance(graph_name, str):
        try:
            graph = collection.data[graph_name]
        except KeyError:
            graph = collection.konect_data[graph_name]
            graph.set_directed(False)
            stats.remove_self_loops(graph)
            stats.remove_parallel_edges(graph)
    else:
        graph = gt.load_graph(str(graph_name))
        graph_name = graph_name.name.replace(".gt", "")

    graph = topology.extract_largest_component(graph, prune=True)

    return graph_name, graph


def prettify_table(table, columns):
    # adds vertical lines
    table = table.replace("{l%s}" % ("r" * columns), "{|l||%s}" % ("r|" * columns))
    table = table.replace("{l%s}" % ("l" * columns), "{|l||%s}" % ("r|" * columns))

    # add horizontal lines
    table = table.replace("\\\\\n", "\\\\ \\hline \n")

    # prettier header column line
    table = table.replace("\\hline \n\\hline", "\n\\hhline{|=||%s}" % ("=|" * columns), 1)

    # remove double bottom border
    table = table.replace("\\hline \n\\hline", "\\hline")

    return table
