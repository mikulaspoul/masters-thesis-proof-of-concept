"""
This file contains the seaborn config for plotting.
"""
import seaborn as sns
sns.set(style="whitegrid")
sns.set_palette("colorblind")
