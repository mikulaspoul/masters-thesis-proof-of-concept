# Proof of Concept code for my Master's Thesis

This is a repo for code relating to my Master's Thesis (all info available [at this site](https://masters-thesis.mikulaspoul.cz/)).

## Installation guide:

  1. [Compile graph-tool](https://git.skewed.de/count0/graph-tool/wikis/installation-instructions#manual-compilation)
  2. `python3 -m venv --system-site-packages --copies`
  3. `source venv/bin/activate`
  4. `pip install -r requirements.txt` 

## Files in this repository

Each file includes a comment at the top with description of content and purpose.

  - `figures/` - various generated plots
  - `outputs/` - raw results of experiments + some processed results as well
  - `springbok/` - the implementation of the speedup and approximation in C++ and Python. See README in that folder.
  - `tables/` - some generated tables
  - `test_graphs/` - location for test graphs (files used in the thesis available [here](https://school.mikulaspoul.cz/masters-thesis/))
  - `plot_*` - files relating to plotting results
  - `sampling_*` - files relating to running sampling experiments
  - `sns_config.py` - plotting configuration
  - `speedup_*` - files relating to running speedup experiments
  - `table_*` - files relating to creating tables with results
  - `test_graphs_*` - generating and processing test graphs and their weights
  - `test_*` - testing various aspects of springbok
  - `runner.sh` - helper file for running sampling experiments
  - `requirements.txt` - the packages needed to run the various scripts
  - `Makefile` - just a proxy to springbok's Makefile
