"""
This file tests the 2-chain detection.
"""
import graph_tool as gt
import numpy as np

from springbok import _springbok as sb
from springbok import libspringbok

graph = gt.Graph(directed=False)
graph.add_edge(0, 1)
graph.add_edge(0, 3)
graph.add_edge(1, 2)
graph.add_edge(2, 5)
graph.add_edge(3, 4)
graph.add_edge(3, 6)
graph.add_edge(3, 11)
graph.add_edge(3, 14)
graph.add_edge(4, 6)
graph.add_edge(4, 5)
graph.add_edge(4, 7)
graph.add_edge(5, 10)
graph.add_edge(5, 12)
graph.add_edge(5, 13)
graph.add_edge(6, 7)
graph.add_edge(6, 8)
graph.add_edge(7, 8)
graph.add_edge(7, 12)
graph.add_edge(7, 13)
graph.add_edge(8, 9)
graph.add_edge(8, 11)
graph.add_edge(8, 15)
graph.add_edge(9, 10)
graph.add_edge(14, 15)

graph.add_edge(15, 16)
graph.add_edge(17, 18)
graph.add_edge(17, 19)


weights = graph.new_edge_property("float")
weights.a = np.ones(graph.num_edges())

graph.properties[("e", sb.WEIGHTS_PROPERTY_NAME)] = weights

onecore_filter_nodes = graph.new_vertex_property("bool")
onecore_filter_results = libspringbok.filter_1core(
    graph._Graph__graph,
    gt._prop("v", graph, onecore_filter_nodes),
    gt._prop("e", graph, graph.ep[sb.WEIGHTS_PROPERTY_NAME]),
    True
)

twochain_filter_nodes = graph.new_vertex_property("bool")
twochain_filter_results = libspringbok.filter_2chain(
    gt.GraphView(graph, onecore_filter_nodes.a == False)._Graph__graph,
    gt._prop("v", graph, twochain_filter_nodes),
    gt._prop("e", graph, graph.ep[sb.WEIGHTS_PROPERTY_NAME]),
)

(twochain_nodes, twochain_ids,
 twochain_parents_left, twochain_parents_right,
 twochain_distances_left, twochain_distances_right, twochain_distance) = twochain_filter_results

print(twochain_nodes)
print(twochain_ids)
print(twochain_parents_left)
print(twochain_parents_right)
print(twochain_distances_left)
print(twochain_distances_right)
print(twochain_distance)
