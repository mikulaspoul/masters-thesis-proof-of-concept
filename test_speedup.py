"""
This file tests the improved APL algorithm against the naive one, with speed as well.
"""
import numpy as np
from graph_tool import collection, topology

import springbok
from springbok.utils import measure

graph = collection.data["adjnoun"]
graph = topology.extract_largest_component(graph, prune=True)

weights = graph.new_edge_property("float")
weights.a = np.ones(graph.num_edges(), dtype=np.float)

print("NO WEIGHTS")
with measure("dumb", silent=False):
    print(springbok.naive_average_path_length(graph))
print("")
with measure("clever", silent=False):
    print(springbok.average_path_length(graph))
print("---------------\n\n")

print("WEIGHTS")
with measure("dumb", silent=False):
    print(springbok.naive_average_path_length(graph, weights=weights))
print("")
with measure("clever", silent=False):
    print(springbok.average_path_length(graph, weights=weights))
